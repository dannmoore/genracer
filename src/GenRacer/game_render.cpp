/*
  File:     game_render.cpp
  Description: Game class rendering methods
  Notes: SFML does not support native z-layering, so make render calls back-to-front

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"





/*
mSetWindowHandle():
	Sets the window handle for our renderer to use

Params:
	*w - A pointer to an SF::Window
*/
void Game::mSetWindowHandle(sf::RenderWindow *w) {
	mainWindow = w;
}



void Game::mUpdateScalingFactor() {
	// Update texture scaling factor
	mScaleFactorX = static_cast<float>(mainWindow->getSize().x) / static_cast<float>(texBuf.getSize().x);
	mScaleFactorY = static_cast<float>(mainWindow->getSize().y) / static_cast<float>(texBuf.getSize().y);
}




/*
mRenderFrame():
	Renders a frame
*/
void Game::mRenderFrame() {
	mainWindow->clear();

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		mRenderMainMenu();
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		mRenderBG();
		mRenderCars();
		mRenderOnScreenText();

		if (bShowCarDebug) {
			// Show information about the selected AI car on screen
			mRenderCarAIRays(mHighlightedCar);
			mRenderCarAIDebug(mHighlightedCar);
		}
		break;
	case Game::__eGAMESTATE::__ENDOFROUND:
		mRenderBG();
		mRenderCars();
		mRenderOnScreenText();

		if (bShowCarDebug) {
			// Show information about the selected AI car on screen
			mRenderCarAIRays(mHighlightedCar);
			mRenderCarAIDebug(mHighlightedCar);

			// Show information about AI fitness
			mRenderCarAIFitness();
		}
		break;
	default:
		break;
	}

	mRenderBuf();

	mainWindow->display();
}


/*
mRenderBuf():
	Renders our buffer texture to the main window
*/

void Game::mRenderBuf() {
	texBuf.display(); // Must call display after drawing on a texture or the result isn't oriented correctly

	sf::Texture tmpTex;
	sf::Sprite tmpSprite; // A temporary sprite for drawing
	tmpTex = texBuf.getTexture();
	tmpSprite.setTexture(tmpTex);
	tmpSprite.setTextureRect(sf::IntRect(0, 0, tmpTex.getSize().x, tmpTex.getSize().y));
	tmpSprite.setPosition(0, 0);

	tmpSprite.setScale(
		mScaleFactorX,
		mScaleFactorY
	);


	mainWindow->draw(tmpSprite);
}


/*
mRenderMainMenu():
	Draw the Setup Menu
*/

void Game::mRenderMainMenu() {
	sf::FloatRect r;
	std::string str;
	sf::Text text;


	// solid background color
	texBuf.clear(sf::Color(162, 162, 162));



	text.setFont(fntDebug);

	str = "Setup";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - (r.width / 2), 0));	// top-center alignment

	texBuf.draw(text);


	// Instructional Text
	str = "ESC - Exit Application";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 32));	// bottom-right-alignment

	texBuf.draw(text);


	str = "Space - Begin AI Generation";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 64));	// bottom-right-alignment

	texBuf.draw(text);


	str = "Arrow Keys - Navigate and Change Values";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 96));	// bottom-right-alignment

	texBuf.draw(text);






	// AI Options
	str = "Number Of AI Cars: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if(mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPNUMCARS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 128));	// center alignment

	texBuf.draw(text);


	str = std::to_string(mSetupNumCars);
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPNUMCARS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 128));	// center alignment

	texBuf.draw(text);


	str = "Generation Round Time: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPROUNDTIME) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 192));	// center alignment

	texBuf.draw(text);


	str = std::to_string(mSetupRoundTime);
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPROUNDTIME) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 192));	// center alignment

	texBuf.draw(text);



	str = "Fit Parents To Select: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPFITPARENTS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 256));	// center alignment

	texBuf.draw(text);


	str = std::to_string(mSetupFitParents);
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPFITPARENTS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 256));	// center alignment

	texBuf.draw(text);



	str = "Non-Fit Parents To Select: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPNONFITPARENTS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 320));	// center alignment

	texBuf.draw(text);


	str = std::to_string(mSetupNonFitParents);
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPNONFITPARENTS) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 320));	// center alignment

	texBuf.draw(text);


	str = "Mutation Rate: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPMUTATIONRATE) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 384));	// center alignment

	texBuf.draw(text);


	
	str = std::to_string(mSetupMutationRate) + "%";

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPMUTATIONRATE) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 384));	// center alignment

	texBuf.draw(text);




	str = "Track Selection: ";
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPTRACK) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f) - r.width, 448));	// center alignment

	texBuf.draw(text);


	if (mSetupSelectedTrack == 0) str = "All"; // 0 is a special case and selects all tracks
	else str = "Track " + std::to_string(mSetupSelectedTrack);
	
	text.setString(str);
	text.setCharacterSize(32); // in pixels
	if (mSetupSelectedRow == Game::__eSETUPMENU::__eSETUPTRACK) text.setFillColor(sf::Color::White);
	else text.setFillColor(sf::Color::Magenta);

	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f((texBuf.getSize().x / 2.0f), 448));	// center alignment

	texBuf.draw(text);

}



/*
mRenderOnScreenText():
	Draw instructions and status text
*/

// Draw instructions and status text
void Game::mRenderOnScreenText() {
	sf::FloatRect r;
	std::string str;


	sf::Text text;

	text.setFont(fntDebug);

	// Draw some instructional text
	if (mGameState == Game::__eGAMESTATE::__MAINVIEW) {
		str = "ESC - Return to Setup";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 32));	// bottom-right-alignment

		texBuf.draw(text);


		if (bShowCarDebug) str = "I - Hide AI Debug Info";
		else str = "I - Show AI Debug Info";

		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 64));	// bottom-right-alignment

		texBuf.draw(text);




		if (bAutoRun) str = "A - Toggle AutoRun Off";
		else str = "A - Toggle AutoRun On";

		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 96));	// bottom-right-alignment

		texBuf.draw(text);




		str = "CTRL-R - Reset to Gen 0";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 128));	// bottom-right-alignment

		texBuf.draw(text);


		str = "Left Click - Select Car";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 160));	// bottom-right-alignment

		texBuf.draw(text);

	}
	else {
		str = "ESC - Return to Setup";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 32));	// bottom-right-alignment

		texBuf.draw(text);


		if (bShowCarDebug) str = "I - Hide AI Debug Info";
		else str = "I - Show AI Debug Info";

		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 64));	// bottom-right-alignment

		texBuf.draw(text);



		if (bAutoRun) str = "A - Toggle AutoRun Off";
		else str = "A - Toggle AutoRun On";

		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 96));	// bottom-right-alignment

		texBuf.draw(text);


		str = "Space - Next Generation";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 128));	// bottom-right-alignment

		texBuf.draw(text);


		str = "CTRL-R - Reset to Gen 0";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 160));	// bottom-right-alignment

		texBuf.draw(text);


		str = "M - Mark/Unmark Selected";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 192));	// bottom-right-alignment

		texBuf.draw(text);



		str = "Left Click - Select Car";
		text.setString(str);
		text.setCharacterSize(32); // in pixels
		text.setFillColor(sf::Color::Magenta);

		r = text.getLocalBounds();
		text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, texBuf.getSize().y - r.height - 224));	// bottom-right-alignment

		texBuf.draw(text);
	}





	// Generation Header
	text.setFillColor(sf::Color::Blue);
	str = "Generation " + std::to_string(mAIManager.GetCurrentGeneration()) + ", " + std::to_string(mNumCars) + " members. Selecting top " + std::to_string(mFitParents) + " fittest and " + std::to_string(mNonFitParents) + " random parents, " + std::to_string(mMutationRate) + "% mutation.";
	text.setString(str);
	text.setPosition(sf::Vector2f(2, 0));
	texBuf.draw(text);




	// Draw the round timer
	float time = (mRoundTime / 1000.0f) - clkRoundTimer.getElapsedTime().asSeconds();
	if (mGameState == Game::__eGAMESTATE::__MAINVIEW) {
		str = "Round Time Left: " + std::to_string(time) + "s";
	}
	else {
		str = "Round Time Left: 0s";
	}

	text.setString(str);
	text.setCharacterSize(32); // in pixels
	text.setFillColor(sf::Color::Cyan);
	r = text.getLocalBounds();
	text.setPosition(sf::Vector2f(texBuf.getSize().x - r.width - 4, 0));	// right-alignment

	texBuf.draw(text);

}



/*
mRenderBG():
	Draw the background
*/

void Game::mRenderBG() {
	sf::Sprite tmpSprite; // A temporary sprite for drawing
	switch (mCurrentTrack) {
		case 1:
			tmpSprite.setTexture(texTrack1);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texTrack1.getSize().x, texTrack1.getSize().y));
			tmpSprite.setPosition(0, 0);
			break;
		case 2:
			tmpSprite.setTexture(texTrack2);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texTrack2.getSize().x, texTrack2.getSize().y));
			tmpSprite.setPosition(0, 0);
			break;
		case 3:
			tmpSprite.setTexture(texTrack3);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texTrack3.getSize().x, texTrack3.getSize().y));
			tmpSprite.setPosition(0, 0);
			break;
		case 4:
			tmpSprite.setTexture(texTrack4);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texTrack4.getSize().x, texTrack4.getSize().y));
			tmpSprite.setPosition(0, 0);
			break;
		default:
			tmpSprite.setTexture(texTrack1);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texTrack1.getSize().x, texTrack1.getSize().y));
			tmpSprite.setPosition(0, 0);
			break;
	}

	texBuf.draw(tmpSprite);


}


/*
mRenderCars():
	Draw the cars on the track
*/
void Game::mRenderCars() {
	sf::Sprite tmpSprite; // A temporary sprite for drawing

	for (int i = 0; i < mNumCars; i++) {
		if (mCars[i].active) {
			tmpSprite.setTexture(texCar);
			tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
			tmpSprite.setPosition(mCars[i].x, mCars[i].y);
			// Set origin to rotate around center of sprite, note the car will be drawn centered on the origin
			tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
			tmpSprite.setRotation(mCars[i].facing);

			tmpSprite.setColor(sf::Color(mCars[i].r, mCars[i].g, mCars[i].b));

			texBuf.draw(tmpSprite);
		}
	}

}


/*
mRenderCarAIFitness():
	Render the selected cars that will become parents for the next generation
*/
void Game::mRenderCarAIFitness() {
	sf::Sprite tmpSprite; // A temporary sprite
	sf::RectangleShape rectangle;
	sf::FloatRect r;

	for (int i = 0; i < mNumCars; i++) {
		if (mCars[i].active) {
			if (mAIManager.GetFitness(i) == AIManager::__eAIFITNESS::__FIT) { // This car is very fit, draw a green box around it
				tmpSprite.setTexture(texCar);
				tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
				tmpSprite.setPosition(mCars[i].x, mCars[i].y);
				// Set origin to rotate around center of sprite, note the car will be drawn centered on the origin
				tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
				tmpSprite.setRotation(mCars[i].facing);

				r = tmpSprite.getGlobalBounds();
				rectangle.setSize(sf::Vector2f(r.width, r.height));
				rectangle.setPosition(r.left, r.top);
				rectangle.setOutlineThickness(2);
				rectangle.setFillColor(sf::Color::Transparent);
				rectangle.setOutlineColor(sf::Color(50, 250, 50));

				texBuf.draw(rectangle);
			}
			else if (mAIManager.GetFitness(i) == AIManager::__eAIFITNESS::__UNFIT) { // This car is not very fit but was chosen for the next generation
				tmpSprite.setTexture(texCar);
				tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
				tmpSprite.setPosition(mCars[i].x, mCars[i].y);
				// Set origin to rotate around center of sprite, note the car will be drawn centered on the origin
				tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
				tmpSprite.setRotation(mCars[i].facing);

				r = tmpSprite.getGlobalBounds();
				rectangle.setSize(sf::Vector2f(r.width, r.height));
				rectangle.setPosition(r.left, r.top);
				rectangle.setOutlineThickness(2);
				rectangle.setFillColor(sf::Color::Transparent);
				rectangle.setOutlineColor(sf::Color(250, 50, 50));

				texBuf.draw(rectangle);
			}
			else if (mAIManager.GetFitness(i) == AIManager::__eAIFITNESS::__USER) { // This car was chosen by the user for the next generation
				tmpSprite.setTexture(texCar);
				tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
				tmpSprite.setPosition(mCars[i].x, mCars[i].y);
				// Set origin to rotate around center of sprite, note the car will be drawn centered on the origin
				tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
				tmpSprite.setRotation(mCars[i].facing);

				r = tmpSprite.getGlobalBounds();
				rectangle.setSize(sf::Vector2f(r.width, r.height));
				rectangle.setPosition(r.left, r.top);
				rectangle.setOutlineThickness(2);
				rectangle.setFillColor(sf::Color::Transparent);
				rectangle.setOutlineColor(sf::Color(50, 50, 250));

				texBuf.draw(rectangle);
			}
		}
	}
}



/*
mRenderCarAIRays():
	Draw the sensor rays for the specified car

Params:
	id = the car index to draw rays from
*/
void Game::mRenderCarAIRays(int id) {
	sf::Transform transform;
	sf::FloatRect rect;

	sf::Sprite tmpSprite; // A temporary sprite 

	tmpSprite.setTexture(texCar);
	tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
	tmpSprite.setPosition(mCars[id].x, mCars[id].y);
	// Set origin to rotate around center of sprite
	tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
	tmpSprite.setRotation(mCars[id].facing);

	sf::Vector2f frontcenter;
	transform = tmpSprite.getTransform();
	rect = tmpSprite.getLocalBounds();

	frontcenter = transform.transformPoint(sf::Vector2f(rect.left + (rect.width / 2.0f), rect.top));


	
	// Render the rays out from the car
	sf::Vertex line[2];
	sf::Vector2f endpoint;

	// front-left, 90 degrees to left
	endpoint = mCastRay(frontcenter, mCars[id].facing - 90);
	line[0] = sf::Vertex(frontcenter, sf::Color(50, 250, 50));
	line[1] = sf::Vertex(endpoint, sf::Color(50, 250, 50));

	texBuf.draw(line, 2, sf::Lines);

	// front-left, 45 degrees to left
	endpoint = mCastRay(frontcenter, mCars[id].facing - 45);
	line[0] = sf::Vertex(frontcenter, sf::Color(50, 250, 50));
	line[1] = sf::Vertex(endpoint, sf::Color(50, 250, 50));

	texBuf.draw(line, 2, sf::Lines);

	// front-center, forward
	endpoint = mCastRay(frontcenter, mCars[id].facing);
	line[0] = sf::Vertex(frontcenter, sf::Color(50, 250, 50));
	line[1] = sf::Vertex(endpoint, sf::Color(50, 250, 50));

	texBuf.draw(line, 2, sf::Lines);

	// front-right, 45 degrees to right
	endpoint = mCastRay(frontcenter, mCars[id].facing + 45);
	line[0] = sf::Vertex(frontcenter, sf::Color(50, 250, 50));
	line[1] = sf::Vertex(endpoint, sf::Color(50, 250, 50));

	texBuf.draw(line, 2, sf::Lines);


	// front-right, 90 degrees to right
	endpoint = mCastRay(frontcenter, mCars[id].facing + 90);
	line[0] = sf::Vertex(frontcenter, sf::Color(50, 250, 50));
	line[1] = sf::Vertex(endpoint, sf::Color(50, 250, 50));

	texBuf.draw(line, 2, sf::Lines);
}



/*
mRenderCarAIDebug():
	Draw debug information about the specified car on the screen

Params:
	id = The car to display information about
*/
void Game::mRenderCarAIDebug(int id) {
	sf::Text text;
	std::string str; //string buffer to convert numbers to string

	text.setFont(fntDebug);
	text.setCharacterSize(32); // in pixels

	text.setFillColor(sf::Color::Cyan);


	// Car Header
	str = "Car [" + std::to_string(id) + "] Rays: (measured / threshold)";
	text.setString(str);
	text.setPosition(sf::Vector2f(2, 32));
	texBuf.draw(text);


	// Sensor information
	str = " LEFT: " + std::to_string(mAIManager.GetMeasurement(id, 0)) + " / " + std::to_string(mAIManager.GetThreshold(id, 0));
	text.setString(str);
	text.setPosition(sf::Vector2f(0, 64));
	texBuf.draw(text);

	str = " FRONT-LEFT: " + std::to_string(mAIManager.GetMeasurement(id, 1)) + " / " + std::to_string(mAIManager.GetThreshold(id, 1));
	text.setString(str);
	text.setPosition(sf::Vector2f(0, 96));
	texBuf.draw(text);

	str = " FRONT: " + std::to_string(mAIManager.GetMeasurement(id, 2)) + " / " + std::to_string(mAIManager.GetThreshold(id, 2));
	text.setString(str);
	text.setPosition(sf::Vector2f(0, 128));
	texBuf.draw(text);

	str = " FRONT-RIGHT: " + std::to_string(mAIManager.GetMeasurement(id, 3)) + " / " + std::to_string(mAIManager.GetThreshold(id, 3));
	text.setString(str);
	text.setPosition(sf::Vector2f(0, 160));
	texBuf.draw(text);

	str = " RIGHT: " + std::to_string(mAIManager.GetMeasurement(id, 4)) + " / " + std::to_string(mAIManager.GetThreshold(id, 4));
	text.setString(str);
	text.setPosition(sf::Vector2f(0, 192));
	texBuf.draw(text);


	// Action information (just pull from mCars for current actions)
	if (mCars[id].accelerating) str = "Accel: True";
	else str = "Accel: False";
	text.setString(str);
	text.setPosition(sf::Vector2f(640, 32));
	texBuf.draw(text);

	if (mCars[id].braking) str = "Brake: True";
	else str = "Brake: False";
	text.setString(str);
	text.setPosition(sf::Vector2f(640, 64));
	texBuf.draw(text);

	if (mCars[id].turndir < 0) str = "Turning: Left";
	else if (mCars[id].turndir > 0) str = "Turning: Right";
	else str = "Turning: No";
	text.setString(str);
	text.setPosition(sf::Vector2f(640, 96));
	texBuf.draw(text);

	if (mCars[id].crashed) {
		str = "Crashed: True";
		text.setFillColor(sf::Color(255, 128, 0));
	}
	else {
		str = "Crashed: False";
		text.setFillColor(sf::Color::Cyan);
	}
	text.setString(str);
	text.setPosition(sf::Vector2f(640, 128));
	texBuf.draw(text);


	str = "Distance: " + std::to_string(mAIManager.GetDistance(id));
	text.setString(str);
	text.setPosition(sf::Vector2f(640, 160));
	text.setFillColor(sf::Color::Cyan);
	texBuf.draw(text);

	if (mAIManager.GetFitness(id) == AIManager::__eAIFITNESS::__FIT) {
		str = "Fitness: Good";
		text.setFillColor(sf::Color::Green);
	}
	else if (mAIManager.GetFitness(id) == AIManager::__eAIFITNESS::__UNFIT) {
		str = "Fitness: Poor";
		text.setFillColor(sf::Color::Red);
	}
	else if (mAIManager.GetFitness(id) == AIManager::__eAIFITNESS::__USER) {
		str = "Fitness: User Selected";
		text.setFillColor(sf::Color::Blue);
	}
	else {
		str = "Fitness: None";
		text.setFillColor(sf::Color::Cyan);
	}

	text.setString(str);
	text.setPosition(sf::Vector2f(640, 192));
	texBuf.draw(text);

}
