/*
  File:    car.h
  Description: Car class 
  Notes: A single car and associated status information

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#ifndef	__CAR_H_
#define	__CAR_H_



class Car {
public:
	Car();
	~Car();


	float speed;		// In pixels per game tick
	float x;			// x,y position
	float y;
	float facing;		// which direction the car is pointed, in degrees

	bool active;		// Is this car active on the track, should it be rendered?
	bool crashed;		// Has the car crashed and can no longer move?
	bool accelerating;	// Is this car currently accelerating?
	bool braking;		// is this car currently braking?
	int turndir;		// Direction of turning (-1 = left, 0 = not turning, 1 = right)

	int r;	// color values of this car
	int g;
	int b;

private:

};



#endif // __CAR_H_
