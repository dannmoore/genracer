/*
  File:     game_input.cpp
  Description: Game class input methods
  Notes:

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"




void Game::mHandleInput(sf::Event *e) {
	switch (e->type) {
		// Keyboard events
	case sf::Event::KeyPressed: // Key down or repeating
		mInputKeyDown(e->key.code);
		break;
	case sf::Event::KeyReleased: // Key up
		mInputKeyUp(e->key.code);
		break;

		// Mouse events
	case sf::Event::MouseMoved: // Mouse moved within our window
		mMouseMoved(e->mouseMove.x, e->mouseMove.y);
		break;
	case sf::Event::MouseButtonPressed: // Mouse button pressed
		mMouseButtonPressed(e->mouseButton.button);
		break;
	default:
		break;

		// Window events
	case sf::Event::Resized: // Window was resized
		// Set the size of the default view so things get stretched appropriately
		sf::Vector2f newsize; newsize.x = static_cast<float>(mainWindow->getSize().x); newsize.y = static_cast<float>(mainWindow->getSize().y);
		sf::Vector2f newcenter; newcenter.x = newsize.x / 2.0f; newcenter.y = newsize.y / 2.0f;
		mainWindow->setView(sf::View(newcenter, newsize));

		mUpdateScalingFactor();
		break;
	}
}




void Game::mInputKeyDown(sf::Keyboard::Key k) {
	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		switch (k) {
			case sf::Keyboard::Space:
				// Test for track selection
				if (mSetupSelectedTrack == 0) {
					bTrackRotation = true;
				}
				else {
					bTrackRotation = false;
				}
				mClear();
				mReset();
				mGameState = Game::__eGAMESTATE::__MAINVIEW;
				break;

			case sf::Keyboard::Down:
				mSetupSelectedRow++;
				if (mSetupSelectedRow > Game::__eSETUPMENU::__eSETUPTRACK) mSetupSelectedRow = Game::__eSETUPMENU::__eSETUPTRACK;	// constrain menu to bottom element
				break;

			case sf::Keyboard::Up:
				mSetupSelectedRow--;
				if (mSetupSelectedRow < 0) mSetupSelectedRow = 0;	// constrain menu to top element
				break;

			case sf::Keyboard::Left: // reduce value of setup selection
				switch (mSetupSelectedRow) {
					case Game::__eSETUPMENU::__eSETUPNUMCARS:
						mSetupNumCars--;
						if (mSetupNumCars < 2) {
							mSetupNumCars = 2; // enforce minimum
						}

						if (mSetupNumCars < (mSetupFitParents + mSetupNonFitParents)) { // enforce parent count when number of cars < combined parent count
							// attempt to reduce fit parent count
							mSetupFitParents--;
							if (mSetupFitParents < 1) { // enforce miminum
								mSetupFitParents++;
								mSetupNonFitParents--;
							}
						}
						break;
					case Game::__eSETUPMENU::__eSETUPROUNDTIME:
						mSetupRoundTime--;
						if (mSetupRoundTime < 1) mSetupRoundTime = 1; // enforce minimum
						break;
					case Game::__eSETUPMENU::__eSETUPFITPARENTS:
						mSetupFitParents--;
						if (mSetupFitParents < 1) mSetupFitParents = 1; // enforce minimum
						break;
					case Game::__eSETUPMENU::__eSETUPNONFITPARENTS:
						mSetupNonFitParents--;
						if (mSetupNonFitParents < 1) mSetupNonFitParents = 1; // enforce minimum
						break;
					case Game::__eSETUPMENU::__eSETUPTRACK:
						mSetupSelectedTrack--;
						if (mSetupSelectedTrack < 0) mSetupSelectedTrack = 0;
						break;
					case Game::__eSETUPMENU::__eSETUPMUTATIONRATE:
						mSetupMutationRate--;
						if (mSetupMutationRate < 0) mSetupMutationRate = 0;
						break;
					default:
						break;
				}
				break;

			case sf::Keyboard::Right: // increase value of setup selection
				switch (mSetupSelectedRow) {
					case Game::__eSETUPMENU::__eSETUPNUMCARS:
						mSetupNumCars++;
						if (mSetupNumCars > MAX_CARS) mSetupNumCars = MAX_CARS; // enforce maximum
						break;
					case Game::__eSETUPMENU::__eSETUPROUNDTIME:
						mSetupRoundTime++;
						break;
					case Game::__eSETUPMENU::__eSETUPFITPARENTS:
						mSetupFitParents++;
						if ((mSetupFitParents + mSetupNonFitParents) > mSetupNumCars) mSetupFitParents = mSetupNumCars - mSetupNonFitParents; // enforce maximum
						break;
					case Game::__eSETUPMENU::__eSETUPNONFITPARENTS:
						mSetupNonFitParents++;
						if ((mSetupFitParents + mSetupNonFitParents) > mSetupNumCars) mSetupNonFitParents = mSetupNumCars - mSetupFitParents; // enforce maximum
						break;
					case Game::__eSETUPMENU::__eSETUPTRACK:
						mSetupSelectedTrack++;
						if (mSetupSelectedTrack > 4) mSetupSelectedTrack = 4; 
						break;
					case Game::__eSETUPMENU::__eSETUPMUTATIONRATE:
						mSetupMutationRate++;
						if (mSetupMutationRate > 100) mSetupMutationRate = 100;
						break;
					default:
						break;
				}

				break;



			case sf::Keyboard::Escape:
				// Exit application
				shouldclose = true;
				break;

			default:
				break;
		}
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		switch (k) {
			case sf::Keyboard::Left:
				//mSetTurnLeft(0, true);
				break;
			case sf::Keyboard::Right:
				//mSetTurnRight(0, true);
				break;
			case sf::Keyboard::Up:
				//mSetAccelerate(0, true);
				break;
			case sf::Keyboard::Down:
				//mSetBrake(0, true);
				break;
			case sf::Keyboard::R:
				// CTRL+R
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
					mClear();
					mReset();
				}
				break;
			case sf::Keyboard::A:
				bAutoRun = !bAutoRun;
				break;
			case sf::Keyboard::I:
				bShowCarDebug = !bShowCarDebug;
				break;
			case sf::Keyboard::Escape:
				// Return to setup screen
				mClear();
				mReset();
				mGameState = Game::__eGAMESTATE::__MAINMENU;
				break;
			default:
				break;
		} // switch(k)
		break;
	case Game::__eGAMESTATE::__ENDOFROUND:
		switch (k) {
			case sf::Keyboard::R:
				// CTRL+R
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)) {
					mGameState = Game::__eGAMESTATE::__MAINVIEW;
					mClear();
					mReset();
				}
				break;
			case sf::Keyboard::Space:
				// Generate new ai and start a new round
				mAIManager.CreateNextGeneration(mMutationRate, rng);
				mGameState = Game::__eGAMESTATE::__MAINVIEW; 
				if (bTrackRotation) {
					// Rotate to next track in the playlist
					mCurrentTrack++;
					if (mCurrentTrack > 4) mCurrentTrack = 1; // wrap around
				}
				mReset();
				break;
			case sf::Keyboard::M:
				mAIManager.ToggleMarkForNextGen(mHighlightedCar);
				break;
			case sf::Keyboard::A:
				bAutoRun = !bAutoRun;
				break;
			case sf::Keyboard::I:
				bShowCarDebug = !bShowCarDebug;
				break;
			case sf::Keyboard::Escape:
				// Return to setup screen
				mClear();
				mReset();
				mGameState = Game::__eGAMESTATE::__MAINMENU;
				break;
			default:
				break;
		} // switch(k)
		break;
	default:
		break;
	} // switch(mGameState)
}


void Game::mInputKeyUp(sf::Keyboard::Key k) {
	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		switch (k) {
			case sf::Keyboard::Left:
				//mSetTurnLeft(0, false);
				break;
			case sf::Keyboard::Right:
				//mSetTurnRight(0, false);
				break;
			case sf::Keyboard::Up:
				//mSetAccelerate(0, false);
				break;
			case sf::Keyboard::Down:
				//mSetBrake(0, false);
				break;
			default:
				break;
		} // switch(k)
		break;
	case Game::__eGAMESTATE::__ENDOFROUND:
		switch (k) {
		case sf::Keyboard::Left:
			break;
		case sf::Keyboard::Right:
			break;
		case sf::Keyboard::Up:
			break;
		case sf::Keyboard::Down:
			break;
		default:
			break;
		} // switch(k)
		break;
	default:
		break;
	} // switch(mGameState)
}



/*
mMouseMoved():
	Processes a mouse move event

Params:
	x,y - The new x,y position of the mouse cursor relative to our window
*/
void Game::mMouseMoved(int x, int y) {

}


/*
mMouseButtonPressed():
	Process a mouse button event

Params:
	b - The mouse button that was pressed
*/
void Game::mMouseButtonPressed(sf::Mouse::Button b) {
	int x = sf::Mouse::getPosition(*mainWindow).x;
	int y = sf::Mouse::getPosition(*mainWindow).y;

	sf::Sprite tmpSprite;	// temporary sprite
	sf::FloatRect r;

	// Correct for window scaling
	x = static_cast<int>(x / mScaleFactorX);
	y = static_cast<int>(y / mScaleFactorY);

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
	case Game::__eGAMESTATE::__ENDOFROUND:
		if (b == sf::Mouse::Button::Left) {
			// Check to see if we clicked on a car
			for (int i = 0; i < mNumCars; i++) {
				if (mCars[i].active) {
					// Test bounding box for car, for speed we don't need pixel-perfect accuracy, so just get a general bounding box
					tmpSprite.setTexture(texCar);
					tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
					tmpSprite.setPosition(mCars[i].x, mCars[i].y);
					// Set origin to rotate around center of sprite, note the car will be drawn centered on the origin
					tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
					tmpSprite.setRotation(mCars[i].facing);
					r = tmpSprite.getGlobalBounds();
					if (r.contains(static_cast<float>(x), static_cast<float>(y))) {
						mHighlightedCar = i;
						break;	// we can now break out of our loop
					}				
				}
			}
		}

		break;
	default:
		break;
	} // switch(mGameState)
}




/*
mHandleInputStatus():
	Checks stauts of keys and buttons and processes them, to be called every game tick
*/
void Game::mHandleInputStatus() {
	int x = sf::Mouse::getPosition(*mainWindow).x;
	int y = sf::Mouse::getPosition(*mainWindow).y;

	sf::Vector2i v;

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break; // case Game::__eGAMESTATE::__MAINMENU:
	case Game::__eGAMESTATE::__MAINVIEW:
		// Process mouse
		// Test for mouse buttons
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {


		}
		else {

		}
		

		break; // case Game::__eGAMESTATE::__MAINVIEW:


	default:
		break;
	} // switch(mGameState)

}
