/*
  File:     game_texture.cpp
  Description: Game class texture methods
  Notes:

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"


/*
mLoadTextures():
	Loads textures from the file system into memory
*/
void Game::mLoadTextures() {
	// TODO: Error out if file missing/corrupt
	texTrack1.loadFromFile(".\\data\\img\\track1.png");
	texTrackMask1.loadFromFile(".\\data\\img\\track1_mask.png");
	texTrack2.loadFromFile(".\\data\\img\\track2.png");
	texTrackMask2.loadFromFile(".\\data\\img\\track2_mask.png");
	texTrack3.loadFromFile(".\\data\\img\\track3.png");
	texTrackMask3.loadFromFile(".\\data\\img\\track3_mask.png");
	texTrack4.loadFromFile(".\\data\\img\\track4.png");
	texTrackMask4.loadFromFile(".\\data\\img\\track4_mask.png");
	texCar.loadFromFile(".\\data\\img\\car.png");

	texBuf.create(1920, 1080);	// Create our buffer to draw on
}


