/*
  File:     AIMember.cpp
  Description: A single AIMember
  Notes: Contains the various sensor inputs and actions that this AIMember will take based on those inputs

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "AIMember.h"


AIMember::AIMember() {
	// Reset data
	for (int i = 0; i < 5; i++) {
		measurements[i] = 0;
	}

	for (int i = 0; i < 5; i++) {
		thresholds[i] = 0;
	}

	distance_traveled = 0;
	fitness = 0;

	color_r = 255;
	color_g = 255;
	color_b = 255;

	for (int line0 = 0; line0 < 5; line0++) {	// line 0
		for (int thrs0 = 0; thrs0 < 2; thrs0++) {	// < or >
			for (int line1 = 0; line1 < 4; line1++) {	// line 1
				for (int thrs1 = 0; thrs1 < 2; thrs1++) {	// < or >
					for (int line2 = 0; line2 < 3; line2++) {	// line 2
						for (int thrs2 = 0; thrs2 < 2; thrs2++) {	// < or >
							for (int line3 = 0; line3 < 2; line3++) {	// line 3
								for (int thrs3 = 0; thrs3 < 2; thrs3++) {	// < or >
									for (int thrs4 = 0; thrs4 < 2; thrs4++) {	// < or >
										actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = 0;
										actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = 0;
										actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir = 0;
									}	// < or >
								}	// < or >
							}	// line 3
						}	// < or >
					}	// line 2
				}	// < or >
			}	// line 1
		}	// < or >
	}	// line 0

}

AIMember::~AIMember() {

}


void AIMember::MarkFitness(int f) {
	fitness = f;
}

int AIMember::GetFitness() {
	return(fitness);
}

// Add to the distance counter
void AIMember::AddDistance(float dist) {
	distance_traveled += dist;
}

// Get distance traveled
float AIMember::GetDistance() {
	return(distance_traveled);
}

// set the measurements of each of the ray from the car to the edge of the track
void AIMember::SetMeasurements(float m0, float m1, float m2, float m3, float m4) {
	measurements[0] = m0;
	measurements[1] = m1;
	measurements[2] = m2;
	measurements[3] = m3;
	measurements[4] = m4;
}


// set distance traveled to specified amount
void AIMember::SetDistance(float d) {
	distance_traveled = d;
}


// set the color values
void AIMember::SetColor(int r, int g, int b) {
	color_r = r;
	color_g = g;
	color_b = b;
}

// get the red channel value
int AIMember::GetColorR() {
	return(color_r);
}

// get the red channel value
int AIMember::GetColorG() {
	return(color_g);
}

// get the red channel value
int AIMember::GetColorB() {
	return(color_b);
}


// Returns the measurement for the given ray index
float AIMember::GetMeasurement(int index) {
	return(measurements[index]);
}


// Returns the threshold for the given ray index
float AIMember::GetThreshold(int index) {
	return(thresholds[index]);
}

// Based on the current measurements of each ray, determine if the AI will accelerate
bool AIMember::GetAccel() {
	// Some flags for whether the threshold for each ray has been met
	bool threshold_met[5];

	// Set our flags
	for (int i = 0; i < 5; i++) {
		if (measurements[i] < thresholds[i]) threshold_met[i] = true;
		else threshold_met[i] = false;
	}

	// Now we simply consult the actions matrix to determine what the AI will do
	return(actions[0][threshold_met[0]][1][threshold_met[1]][2][threshold_met[2]][3][threshold_met[3]][threshold_met[4]].accel);
}

bool AIMember::GetBrake() {
	// Some flags for whether the threshold for each ray has been met
	bool threshold_met[5];

	// Set our flags
	for (int i = 0; i < 5; i++) {
		if (measurements[i] < thresholds[i]) threshold_met[i] = true;
		else threshold_met[i] = false;
	}

	// Now we simply consult the actions matrix to determine what the AI will do
	return(actions[0][threshold_met[0]][1][threshold_met[1]][2][threshold_met[2]][3][threshold_met[3]][threshold_met[4]].brake);
}

bool AIMember::GetTurnLeft() {
	// Some flags for whether the threshold for each ray has been met
	bool threshold_met[5];

	// Set our flags
	for (int i = 0; i < 5; i++) {
		if (measurements[i] < thresholds[i]) threshold_met[i] = true;
		else threshold_met[i] = false;
	}

	// Now we simply consult the actions matrix to determine what the AI will do
	int result = actions[0][threshold_met[0]][1][threshold_met[1]][2][threshold_met[2]][3][threshold_met[3]][threshold_met[4]].turndir;
	if (result < 0) return(true);
	else return(false);
}

bool AIMember::GetTurnRight() {
	// Some flags for whether the threshold for each ray has been met
	bool threshold_met[5];

	// Set our flags
	for (int i = 0; i < 5; i++) {
		if (measurements[i] < thresholds[i]) threshold_met[i] = true;
		else threshold_met[i] = false;
	}

	// Now we simply consult the actions matrix to determine what the AI will do
	int result = actions[0][threshold_met[0]][1][threshold_met[1]][2][threshold_met[2]][3][threshold_met[3]][threshold_met[4]].turndir;
	if (result > 0) return(true);
	else return(false);
}



// Initializes the ai member with random data (within sane limits)
void AIMember::RandomizeAI(std::mt19937_64 *r) {
	int movement = 0;

	// No measurements taken yet, zero them out
	for (int i = 0; i < 5; i++) {
		measurements[i] = 0;
	}

	// Reset distance traveled
	distance_traveled = 0;

	// Reset fitness category
	fitness = 0;

	// Set a random threshold (in pixels).. the ai will react the measurement[line] is less than threshold[line].  Values in pixels.
	std::uniform_real_distribution<float> unif;	// 0 <= values < 1
	for (int i = 0; i < 5; i++) {
		// for sanity's sake let's choose a minimum value of 1 pixel, and a maximum value of 500 pixels
		thresholds[i] = 1.0f + (unif(*r) * 500.0f);
	}

	std::uniform_int_distribution<int> unii_movement(0, 2);
	std::uniform_int_distribution<int> unii(-1, 1);

	for (int line0 = 0; line0 < 5; line0++) {	// line 0
		for (int thrs0 = 0; thrs0 < 2; thrs0++) {	// < or >
			for (int line1 = 0; line1 < 4; line1++) {	// line 1
				for (int thrs1 = 0; thrs1 < 2; thrs1++) {	// < or >
					for (int line2 = 0; line2 < 3; line2++) {	// line 2
						for (int thrs2 = 0; thrs2 < 2; thrs2++) {	// < or >
							for (int line3 = 0; line3 < 2; line3++) {	// line 3
								for (int thrs3 = 0; thrs3 < 2; thrs3++) {	// < or >
									for (int thrs4 = 0; thrs4 < 2; thrs4++) {	// < or >
										// acceleration and braking are exclusve, or both can be off (coasting)
										movement = unii_movement(*r);
										switch (movement) {
											case 0: // accelerate
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = true;
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = false;
												break;
											case 1: // brake
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = false;
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = true;
												break;
											case 2: // neither
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = false;
												actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = false;
												break;
										}

										// Turning can be -1 (left), 0 (straight), or 1 (right)
										actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir = unii(*r); 
									}	// < or >
								}	// < or >
							}	// line 3
						}	// < or >
					}	// line 2
				}	// < or >
			}	// line 1
		}	// < or >
	}	// line 0
}



// Dump information about this member
void AIMember::PrintDebugInfo() {
	std::cout << "Length of actions[] = " << sizeof(actions) << " elements, Total Bytes = " << sizeof(actions) * sizeof(_actions) << std::endl;

	for (int i = 0; i < 5; i++) {
		std::cout << "thresholds[" << i << "]: " << thresholds[i] << std::endl;
	}

	// uncomment below and change as needed to debug
	//std::cout << "First actions[...] element:" << std::endl;
	//std::cout << "accel = " << actions[0][0][0][0][0][0][0][0][0].accel << " brake = " << actions[0][0][0][0][0][0][0][0][0].brake << " turndir = " << actions[0][0][0][0][0][0][0][0][0].turndir << std::endl;
}
