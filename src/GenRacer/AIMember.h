/*
  File:     AIMember.h
  Description: A single AIMember
  Notes: Contains the various sensor inputs and actions that this AIMember will take based on those inputs

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#ifndef	__AIMEMBER_H_
#define	__AIMEMBER_H_

#include <random>
#include <iostream>


// This represents a single genetically created AI Racer
class AIMember {
public:
	struct _actions {
		int turndir;
		bool accel;
		bool brake;
	};


	AIMember();
	~AIMember();
	void RandomizeAI(std::mt19937_64 *);
	void PrintDebugInfo();
	void SetMeasurements(float, float, float, float, float);
	void SetDistance(float);
	void AddDistance(float);
	void SetColor(int, int, int);
	float GetDistance();
	float GetMeasurement(int);
	float GetThreshold(int);
	bool GetAccel();
	bool GetBrake();
	bool GetTurnRight();
	bool GetTurnLeft();
	int GetColorR();
	int GetColorG();
	int GetColorB();

	void MarkFitness(int);
	int GetFitness();

	/*
		We have a number of inputs to each car, for example a line drawn from the front of the car to the far wall.
		We then also have a threshold on the input, for example the length of the line
		For each state (length of lines > threshold, or length of lines < threshold) each ai has a number of actions available to perform
		The ai will act accordingly, "mate" with other ai's based on their fitness, add mutations, and then attempt again
		Eventually the ai should mutate enough to go around the track without crashing
	*/

	
	float measurements[5]; // 5 lines measured out from the car to the nearest track wall, in pixels
	float thresholds[5]; // 5 thresholds for each line (react early or react late), in this case the ai reacts when measurements[n] < thresholds[n]
	_actions actions[5][2][4][2][3][2][2][2][2]; // each of the lines has two states, and then is compared to the other four lines 
	int color_r; // color values of this member, will be inherited by children ai members
	int color_g;
	int color_b;

private:
	float distance_traveled; // Distance traveled by this member
	int fitness;	// Fitness category for this AI Member
};



#endif // __AIMEMBER_H_
