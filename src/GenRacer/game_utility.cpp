/*
  File: game_utility.cpp
  Description: Game class utility methods
  Notes:

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"


/*
mGetRandomInt():
	Generates a random number

Params:
	low, high - A range of ints between which a number will be generated, inclusive

Returns:
	A randomly generated int
*/
int Game::mGetRandomInt(int low, int high) {
	std::uniform_int_distribution<int> unii(low, high);

	return(unii(*rng));
}


/*
bPtInRect():
	Checks for the given point inside the given rect

Params:
	x,y = point to check
	r = rect to check

Returns:
	true if the given point is within the given rect
*/
bool Game::bPtInRect(int x, int y, sf::IntRect r) {
	if (x >= r.left && x <= (r.left + r.width)
		&& y >= r.top && y <= (r.top + r.height)
		)
	{
		return(true);
	}
	return(false);
}


/*
mCastRay():
	Casts a ray from the given point along the given angle

Params:
	start = point to start from
	angle = the angle to cast the ray

Returns:
	sf::Vector2f - The ending point at which the ray has collided with the track
*/
sf::Vector2f Game::mCastRay(sf::Vector2f start, float angle) {
	sf::Vector2f result(0,0);

	sf::Sprite trackSprite; // A sprite for the track

	switch (mCurrentTrack) {
		case 1:
			trackSprite.setTexture(texTrackMask1);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask1.getSize().x, texTrackMask1.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 2:
			trackSprite.setTexture(texTrackMask2);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask2.getSize().x, texTrackMask2.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 3:
			trackSprite.setTexture(texTrackMask3);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask3.getSize().x, texTrackMask3.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 4:
			trackSprite.setTexture(texTrackMask4);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask4.getSize().x, texTrackMask4.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		default:
			trackSprite.setTexture(texTrackMask1);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask1.getSize().x, texTrackMask1.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
	}

	int startx = static_cast<int>(start.x);
	int starty = static_cast<int>(start.y);

	// calculate an end following the angle that is off-screen (distance=2500 should be good enough - famous last words)
	// sfml's angle starts with 0 pointing straight up then increases clockwise, whereas math functions start with 0 to the right and increasing counter-clockwise, therefore subtract 90 from our given angle
	int endx = startx + static_cast<int>(cos(degreesToRadians(angle - 90)) * 2500);
	int endy = starty + static_cast<int>(sin(degreesToRadians(angle - 90)) * 2500);

	// draw a virtual line from the starting point until we collide with the track (track pixel alpha > 0)
	// bresenham algorithm
	int dx, dy, i, e;
	int incx, incy, inc1, inc2;
	int x, y;

	dx = endx - startx;
	dy = endy - starty;

	if (dx < 0) dx = -dx;
	if (dy < 0) dy = -dy;
	incx = 1;
	if (endx < startx) incx = -1;
	incy = 1;
	if (endy < starty) incy = -1;
	x = startx; y = starty;
	if (dx > dy) {
		// test this pixel to see if it's off-track
		if (Collision::SinglePixelTest(trackSprite, x, y)) {
			result.x = static_cast<float>(x);
			result.y = static_cast<float>(y);
			return(result);
		}

		e = 2 * dy - dx;
		inc1 = 2 * (dy - dx);
		inc2 = 2 * dy;
		for (i = 0; i < dx; i++) {
			if (e >= 0) {
				y += incy;
				e += inc1;
			}
			else
				e += inc2;
			x += incx;

			// test this pixel to see if it's off-track
			if (Collision::SinglePixelTest(trackSprite, x, y)) {
				result.x = static_cast<float>(x);
				result.y = static_cast<float>(y);
				return(result);
			}

		}

	}
	else {
		// test this pixel to see if it's off-track
		if (Collision::SinglePixelTest(trackSprite, x, y)) {
			result.x = static_cast<float>(x);
			result.y = static_cast<float>(y);
			return(result);
		}


		e = 2 * dx - dy;
		inc1 = 2 * (dx - dy);
		inc2 = 2 * dx;
		for (i = 0; i < dy; i++) {
			if (e >= 0) {
				x += incx;
				e += inc1;
			}
			else
				e += inc2;
			y += incy;

			// test this pixel to see if it's off-track
			if (Collision::SinglePixelTest(trackSprite, x, y)) {
				result.x = static_cast<float>(x);
				result.y = static_cast<float>(y);
				return(result);
			}
		}
	}

	return(result);
}


/*
mGetLineLength():
	Calculates the length of a line between two points

Params:
	start = the starting point
	end = the ending point

Returns:
	float, length of the line
*/
float Game::mGetLineLength(sf::Vector2f start, sf::Vector2f end) {
	return(std::sqrt(((start.x - end.x) * (start.x - end.x)) + ((start.y - end.y) * (start.y - end.y))));
}



/*
	mLoadTrackInfo() - Loads track information from disk

	Params:
		infilename = Filename to read from
*/
void Game::mLoadTrackInfo(std::string infilename) {
	std::ifstream infile;
	int val;

	// Open file up for reading
	infile.open(infilename);
	if (infile.fail())
	{
		std::cout << "Error opening " << infilename << "\n";
		return;
	}

	infile >> val;
	mStartingX = static_cast<float>(val);
	infile >> val;
	mStartingY = static_cast<float>(val);
	infile >> val;
	mStartingFacing = static_cast<float>(val);

	infile.close();
}