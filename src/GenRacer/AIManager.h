/*
  File:     AIManager.h
  Description: AIManager class header
  Notes: The AIManager handles all AIMembers, determines parents, breeds and mutates new generations of AIMembers

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/


#ifndef	__AIMANAGER_H_
#define	__AIMANAGER_H_

#include <random>
#include <iostream>

#include "AIMember.h"


#define __AI_PRINT_DEBUG__ 0	// Set to 1 to print debug information about AI Members in console.  Warning, console output will slow down generation significantly!

// TODO: dynamic allocation
#define MAX_AIMEMBERS 100

// Because we have a single mutation % option and so many actions to process, let's define a "fudge factor" to reduce the number of actions
//  that get mutated.  The lower this number, the more action mutations will occur each generation.
#define ACTION_MUTATE_FUDGE_FACTOR 38.4f		
											


// The AIManager class handles all AIMember objects and the genetic algorithms for evolving the members
class AIManager {
public:
	AIManager();
	~AIManager();

	void mReset(int, std::mt19937_64 *);
	void SetMeasurements(int, float, float, float, float, float);
	void SetElapsedTime(int);
	bool GetAccel(int);
	bool GetBrake(int);
	bool GetTurnRight(int);
	bool GetTurnLeft(int);
	int GetFitness(int);
	void AddDistance(int, float);
	float GetDistance(int);
	float GetMeasurement(int, int);
	float GetThreshold(int, int);
	int GetCurrentGeneration();

	void ToggleMarkForNextGen(int);
	
	void SelectFittest(int);
	void SelectRandomNonFit(int n, std::mt19937_64 *);

	void CreateNextGeneration(int, std::mt19937_64 *);
	void BreedNewAIMember(AIMember *, AIMember *, int, int, std::mt19937_64 *);

	enum __eAIFITNESS {
		__NONE = 0,
		__UNFIT = 1,
		__FIT = 2,
		__USER = 3
	};



private:
	AIMember AIMembers[MAX_AIMEMBERS];	// An array to hold our AI members
	AIMember *SortedAIMembers[MAX_AIMEMBERS];	// An array of AIMember pointers for sorting purposes
	int elapsed_time;	// the amount of time elapsed in the current round
	int mNumAIMembers;
	int mCurrentGeneration;
};



#endif // __AIMANAGER_H_
