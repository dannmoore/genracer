/*
  File:     game.cpp
  Description: Game class constructor, general methods
  Notes:

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"

#include <iostream>

Game::Game() {
	shouldclose = false;

	// Initialize RNG
	const unsigned __int64 seed = time(0);

	rng = new std::mt19937_64(seed);

	mGameState = Game::__eGAMESTATE::__MAINMENU;

	bAutoRun = false;
	bShowCarDebug = true;

	mStartingFacing = 0;
	mStartingX = 0;
	mStartingY = 0;

	mCurrentTrack = 1;
	bTrackRotation = false;

	mSetupSelectedRow = Game::__eSETUPMENU::__eSETUPNUMCARS;
	mSetupNumCars = DEFAULT_CARS;
	mSetupRoundTime = DEFAULT_ROUND_SECONDS;
	mSetupFitParents = DEFAULT_FIT_PARENTS;
	mSetupNonFitParents = DEFAULT_NONFIT_PARENTS;
	mSetupSelectedTrack = DEFAULT_SELECTED_TRACK;
	mSetupMutationRate = DEFAULT_MUTATION_RATE;
	
	mClear();
	mReset();

	clkUpdate.restart();
}


Game::~Game() {
	delete rng;
}


bool Game::mShouldClose() {
	return(shouldclose);
}


/*
	mClear() - Clears the data for cars and AI, starts over at Generation 0
*/
void Game::mClear() {
	int r, g, b;
	std::string filename;
	std::uniform_int_distribution<int> unii(20, 255); // use 20-255 for RGB values to avoid pure-black cars

	mNumCars = mSetupNumCars;	// Use the user-selected number of cars
	mRoundTime = mSetupRoundTime * 1000; // convert selected round time to milliseconds
	mFitParents = mSetupFitParents;
	mNonFitParents = mSetupNonFitParents;
	mMutationRate = mSetupMutationRate;

	if (bTrackRotation) mCurrentTrack = 1; // If we are rotating through tracks, start at track 1
	else mCurrentTrack = mSetupSelectedTrack;

	// Load track information
	if (mCurrentTrack != 0) {
		filename = ".\\data\\img\\track" + std::to_string(mCurrentTrack) + ".txt";
		mLoadTrackInfo(filename);
	}
	else {
		filename = ".\\data\\img\\track1.txt";
		mLoadTrackInfo(filename);
	}

	for (int i = 0; i < mNumCars; i++) {
		mCars[i].speed = 0;
		mCars[i].x = mStartingX;
		mCars[i].y = mStartingY;
		mCars[i].facing = mStartingFacing;
		mCars[i].active = true;
		mCars[i].crashed = false;

		// Generate random colors for generation 0 cars
		r = unii(*rng);
		g = unii(*rng);
		b = unii(*rng);
		mCars[i].r = r;
		mCars[i].g = g;
		mCars[i].b = b;
	}

	// Reset our AIManager
	mAIManager.mReset(mNumCars, rng);
	mAIManager.SetElapsedTime(0);

	mHighlightedCar = 0;
}


/*
	mReset() - Resets in order to start the race again
*/
void Game::mReset() {
	std::string filename;

	// Load track information
	if (mCurrentTrack != 0) {
		filename = ".\\data\\img\\track" + std::to_string(mCurrentTrack) + ".txt";
		mLoadTrackInfo(filename);
	}
	else {
		filename = ".\\data\\img\\track1.txt";
		mLoadTrackInfo(filename);
	}

	for (int i = 0; i < mNumCars; i++) {
		mCars[i].speed = 0;
		mCars[i].x = mStartingX;
		mCars[i].y = mStartingY;
		mCars[i].facing = mStartingFacing;
		mCars[i].active = true;
		mCars[i].crashed = false;
	}

	// Reset our AIManager
	mAIManager.SetElapsedTime(0);

	mHighlightedCar = 0;

	clkRoundTimer.restart();
}


/*
mUpdate() - Called each frame to update the Game object
*/

void Game::mUpdate() {
	// Check input status
	mHandleInputStatus();

	switch (mGameState) {
	case Game::__eGAMESTATE::__MAINMENU:
		break;
	case Game::__eGAMESTATE::__MAINVIEW:
		// Only update if our clock has fired
		if (clkUpdate.getElapsedTime().asMilliseconds() > T_UPDATE_MS) {
			// Update the AI members
			// We will need to calculate collision rays coming from each car so the AI can make its decisions
			sf::Transform transform;
			sf::FloatRect rect;
			sf::Vector2f frontcenter;
			sf::Vector2f endpoint0;	// our end points
			sf::Vector2f endpoint1; //          1   2   3         <-- ray indicies
			sf::Vector2f endpoint2; //           \  |  /
			sf::Vector2f endpoint3; //      0______ . ______4     <-- front of car
			sf::Vector2f endpoint4; //              ^             <-- frontcenter point

			sf::Sprite tmpSprite; // A temporary sprite 

			bool doaccel;
			bool dobrake;
			bool doturnleft;
			bool doturnright;


			for (int i = 0; i < mNumCars; i++) {
				if (mCars[i].active && !mCars[i].crashed) {
					// First let's get the starting points of the given ai car 
					tmpSprite.setTexture(texCar);
					tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
					tmpSprite.setPosition(mCars[i].x, mCars[i].y);
					// Set origin to rotate around center of sprite
					tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
					tmpSprite.setRotation(mCars[i].facing);

					transform = tmpSprite.getTransform();
					rect = tmpSprite.getLocalBounds();
					frontcenter = transform.transformPoint(sf::Vector2f(rect.left + (rect.width / 2.0f), rect.top));	// Convert the local point on the sprite to the rotated point on screen

					// Now cast rays from the starting point outward at each angle
					endpoint0 = mCastRay(frontcenter, mCars[i].facing - 90);
					endpoint1 = mCastRay(frontcenter, mCars[i].facing - 45);
					endpoint2 = mCastRay(frontcenter, mCars[i].facing);
					endpoint3 = mCastRay(frontcenter, mCars[i].facing + 45);
					endpoint4 = mCastRay(frontcenter, mCars[i].facing + 90);

					mAIManager.SetMeasurements(i,
						mGetLineLength(frontcenter, endpoint0),
						mGetLineLength(frontcenter, endpoint1),
						mGetLineLength(frontcenter, endpoint2),
						mGetLineLength(frontcenter, endpoint3),
						mGetLineLength(frontcenter, endpoint4)
					);

					// Let the AI make a decision based off of its sensor information
					doaccel = mAIManager.GetAccel(i);
					dobrake = mAIManager.GetBrake(i);
					doturnleft = mAIManager.GetTurnLeft(i);
					doturnright = mAIManager.GetTurnRight(i);

					// And now assign the AI's chosen actions to the car
					mCars[i].accelerating = doaccel;
					mCars[i].braking = dobrake;
					if (doturnleft) mCars[i].turndir = -1;
					else if (doturnright) mCars[i].turndir = 1;
					else mCars[i].turndir = 0;
				}
			}


			

			// loop through cars and update their status
			for (int i = 0; i < mNumCars; i++) {
				if (mCars[i].active && !mCars[i].crashed) {
					if (mCars[i].turndir == -1) mCars[i].facing -= CAR_TURN_SPEED;
					else if (mCars[i].turndir == 1) mCars[i].facing += CAR_TURN_SPEED;

					if (mCars[i].accelerating) mCars[i].speed += CAR_ACCEL_SPEED;
					if (mCars[i].braking) mCars[i].speed -= CAR_BRAKE_SPEED;

					// cap car speed
					if (mCars[i].speed > CAR_MAX_SPEED) mCars[i].speed = CAR_MAX_SPEED;
					if (mCars[i].speed < -CAR_MAX_REVERSESPEED) mCars[i].speed = -CAR_MAX_REVERSESPEED;

					// update car position
					float newx; float newy;
					newy = -(mCars[i].speed * static_cast<float>(cos(degreesToRadians(mCars[i].facing))));
					newx = mCars[i].speed * static_cast<float>(sin(degreesToRadians(mCars[i].facing)));
					mCars[i].x += newx;
					mCars[i].y += newy;

					// test for collision with track boundries
					if (mTestTrackCollision(i)) mCars[i].crashed = true;

					// Update the ai's distance traveled for this car (positive if going forward, negative if going backward)
					mAIManager.AddDistance(i, mCars[i].speed);					
				}
			}

			clkUpdate.restart();
		}

		// Check for expiration of the round
		if (clkRoundTimer.getElapsedTime().asMilliseconds() > mRoundTime) {
			mAIManager.SetElapsedTime(mRoundTime);

			// Select the top N fittest AI members
			mAIManager.SelectFittest(mFitParents);
			mAIManager.SelectRandomNonFit(mNonFitParents, rng);

			// check for autorun
			if (bAutoRun) {
				// Generate new ai and start a new round
				mAIManager.CreateNextGeneration(mMutationRate, rng);

				if (bTrackRotation) {
					// Rotate to next track in the playlist
					mCurrentTrack++;
					if (mCurrentTrack > 4) mCurrentTrack = 1; // wrap around
				}
				mReset();
			}
			else {
				// change game state to pause at end of round
				mGameState = Game::__eGAMESTATE::__ENDOFROUND;
			}

		}
		break;

	case Game::__eGAMESTATE::__ENDOFROUND:

		break;
	default:
		break;
	}
	
}


void Game::mSetTurnLeft(int id, bool val) {
	if (id < mNumCars) {	// sanity check
		if(val) mCars[id].turndir = -1;
		else mCars[id].turndir = 0;
	}
}


void Game::mSetTurnRight(int id, bool val) {
	if (id < mNumCars) {	// sanity check
		if (val) mCars[id].turndir = 1;
		else mCars[id].turndir = 0;
	}
}


void Game::mSetAccelerate(int id, bool val) {
	if (id < mNumCars) {	// sanity check
		if(val) mCars[id].accelerating = true;
		else mCars[id].accelerating = false;
	}
}


void Game::mSetBrake(int id, bool val) {
	if (id < mNumCars) {	// sanity check
		if(val) mCars[id].braking = true;
		else mCars[id].braking = false;
	}
}





/*
mTestTrackCollision():
	Test for collision with track bounds for given car id

Params:
	id = The car index to test

Returns:
	true if the car has collided with the track
*/


// Test for collision with track bounds for given car id
bool Game::mTestTrackCollision(int id) {
	sf::Sprite tmpSprite; // A temporary sprite 

	tmpSprite.setTexture(texCar);
	tmpSprite.setTextureRect(sf::IntRect(0, 0, texCar.getSize().x, texCar.getSize().y));
	tmpSprite.setPosition(mCars[id].x, mCars[id].y);
	// Set origin to rotate around center of sprite
	tmpSprite.setOrigin(tmpSprite.getTextureRect().width / 2.0f, tmpSprite.getTextureRect().height / 2.0f);
	tmpSprite.setRotation(mCars[id].facing);

	sf::Sprite trackSprite; // A sprite for the track
	switch (mCurrentTrack) {
		case 1:
			trackSprite.setTexture(texTrackMask1);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask1.getSize().x, texTrackMask1.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 2:
			trackSprite.setTexture(texTrackMask2);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask2.getSize().x, texTrackMask2.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 3:
			trackSprite.setTexture(texTrackMask3);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask3.getSize().x, texTrackMask3.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		case 4:
			trackSprite.setTexture(texTrackMask4);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask4.getSize().x, texTrackMask4.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
		default:
			trackSprite.setTexture(texTrackMask1);
			trackSprite.setTextureRect(sf::IntRect(0, 0, texTrackMask1.getSize().x, texTrackMask1.getSize().y));
			trackSprite.setPosition(0, 0);
			break;
	}


	if (Collision::PixelPerfectTest(tmpSprite, trackSprite)) return(true);
	else return(false);
}

