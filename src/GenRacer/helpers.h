/*
  File:     helpers.h
  Description: Various helper macros and functions
  Notes: 

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#pragma once

#ifndef	__HELPERS_H_
#define	__HELPERS_H_


// constants
#define __PI__ 3.14159265


// macros
#define __SAFE_DELETE(p) 		{ if(p) { delete (p); (p)=NULL; } }			// delete macro
#define __SAFE_DELETE_ARRAY(p) 	{ if(p) { delete [] (p); (p)=NULL; } }		// delete array macro


#define degreesToRadians(angleDegrees) ((angleDegrees) * __PI__ / 180.0)
#define radiansToDegrees(angleRadians) ((angleRadians) * 180.0 / __PI__)



#endif // __HELPERS_H_