/*
  File:     game_sound.cpp
  Description: Game sound input methods
  Notes: Sound not used in this application, commented out for future expansion

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"

/*
mLoadSounds():
	Loads sounds from the file system into memory
*/
void Game::mLoadSounds() {
	// TODO: Handle error if file missing/corrupt
	//mSoundBuffers[Game::__SOUNDSAMPLES::__SND_STARTTURN].loadFromFile(".\\data\\snd\\turn.wav");

	//mSounds[Game::__SOUNDSAMPLES::__SND_STARTTURN].setBuffer(mSoundBuffers[Game::__SOUNDSAMPLES::__SND_STARTTURN]);
}

/*
mPlaySound():
	Plays the specified sound buffer

Params:
	bufindex - The index of our sndBuffer to play
*/
void Game::mPlaySound(Game::__SOUNDSAMPLES bufindex) {
	mSounds[bufindex].play();
}