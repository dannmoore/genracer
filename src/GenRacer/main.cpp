/*
  File:     main.cpp
  Description: Application main()
  Notes: 

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "game.h"



Game *g;	// Our main game object pointer


int main()
{
	g = new Game();
	g->mLoadTextures();
	g->mCreateSprites();
	g->mLoadSounds();
	g->mLoadFonts();

	sf::RenderWindow window(sf::VideoMode(640, 480), "GenRacer", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize);
	// the below will use a fullscreen mode
	//sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "GenRacer", sf::Style::Fullscreen);
	window.setFramerateLimit(60);

	g->mSetWindowHandle(&window);
	g->mUpdateScalingFactor();


	while (window.isOpen())
	{
		// TODO: handle errors properly
		sf::err().rdbuf(NULL);	// This call is here to suppress DirectInput errors from SFML regarding joystick input, may need to dig into SFML source code to find a solution

		sf::Event event;
		while (window.pollEvent(event))
		{
			// system events
			if (event.type == sf::Event::Closed)
				window.close();

			// window events
			if (event.type == sf::Event::Resized) {
				g->mHandleInput(&event);
			}

			// keyboard events
			else if (event.type == sf::Event::KeyPressed) {
				g->mHandleInput(&event);
			}
			else if (event.type == sf::Event::KeyReleased) {
				g->mHandleInput(&event);
			}

			// mouse events
			else if (event.type == sf::Event::MouseMoved) {
				g->mHandleInput(&event);
			}
			else if (event.type == sf::Event::MouseButtonPressed) {
				g->mHandleInput(&event);
			}
		}


		g->mUpdate();
		g->mRenderFrame();

		// Test for application exit
		if (g->mShouldClose()) window.close();

	}


	__SAFE_DELETE(g);


	return 0;
}
