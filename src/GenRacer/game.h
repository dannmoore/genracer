/*
  File:     game.h
  Description: Game class header
  Notes: 

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/


#ifndef	__GAME_H_
#define	__GAME_H_

#include <random>
#include <ctime>
#include <fstream>
#include <iostream>
#include <list>
#include <vector>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "helpers.h"
#include "car.h"
#include "Collision.h"
#include "AIManager.h"


// Game Constants and Defines
#define MAX_SOUNDS	100			// Maximum number of sounds available

// TODO: dynamic allocation
#define MAX_CARS 100			// Maximum number of cars at one time
#define CAR_TURN_SPEED 5.0f		// degrees per tick a car can turn
#define CAR_ACCEL_SPEED 0.25f		// How fast does a car accelerate per tick
#define CAR_BRAKE_SPEED 0.50f		// How fast does a car brake per tick
#define CAR_MAX_SPEED 14.0f		// Maximum speed of a car (forwards)
#define CAR_MAX_REVERSESPEED 7.0f		// Maximum speed of a car (backwards)

#define T_UPDATE_MS 16			// approximately 60fps (should be 16.6ms but we need an integer)
#define DEFAULT_ROUND_SECONDS 15	// Default length of each round for AI cars (in seconds)
#define DEFAULT_CARS 75			// Default number of AI cars
#define DEFAULT_FIT_PARENTS 15	// Default number of fit parents to select
#define DEFAULT_NONFIT_PARENTS 5 // Default number of non-fit parents to select
#define DEFAULT_SELECTED_TRACK 0 // Default track to select
#define DEFAULT_MUTATION_RATE 8 // Default mutation rate (in 1/100% increments, so 0-100 = 0-1%)



class Game {
public:
	Game();
	~Game();

	bool mShouldClose();

private:
	bool shouldclose;



	// Utility
public:
	std::mt19937_64 *rng;	// Random number generator
	int mGetRandomInt(int, int);

	bool bPtInRect(int, int, sf::IntRect);

	float mGetLineLength(sf::Vector2f, sf::Vector2f);
	sf::Vector2f mCastRay(sf::Vector2f, float);

	void mLoadTrackInfo(std::string);

private:



	// Rendering
public:
	void mSetWindowHandle(sf::RenderWindow *);
	void mUpdateScalingFactor();
	void mRenderFrame();

	void mRenderBuf();

	void mRenderMainMenu();
	void mRenderBG();
	void mRenderCars();
	void mRenderOnScreenText();

	void mRenderCarAIRays(int);
	void mRenderCarAIDebug(int id);
	void mRenderCarAIFitness();


	float mScaleFactorX;	// Scaling factor for the render buffer vs window size
	float mScaleFactorY;


private:
	sf::RenderWindow *mainWindow;



	// Fonts and text
public:
	void mLoadFonts();



private:
	sf::Font fntDebug;	// Debug font





// Textures and Sprites
public:
	void mLoadTextures();
	void mCreateSprites();


private:
	sf::Texture texTrack1;
	sf::Texture texTrackMask1;
	sf::Texture texTrack2;
	sf::Texture texTrackMask2;
	sf::Texture texTrack3;
	sf::Texture texTrackMask3;
	sf::Texture texTrack4;
	sf::Texture texTrackMask4;
	sf::Texture texCar;

	sf::RenderTexture texBuf;



	// Sound
public:
	enum __SOUNDSAMPLES {
		__SND_STARTTURN = 0
	};

	void mLoadSounds();
	void mPlaySound(Game::__SOUNDSAMPLES);



private:
	sf::SoundBuffer mSoundBuffers[MAX_SOUNDS];
	sf::Sound mSounds[MAX_SOUNDS];




	// Input
public:
	void mHandleInput(sf::Event *);
	void mInputKeyDown(sf::Keyboard::Key);
	void mInputKeyUp(sf::Keyboard::Key);
	void mMouseButtonPressed(sf::Mouse::Button);
	void mMouseMoved(int, int);
	void mHandleInputStatus();




private:



	// Gameplay
public:
	enum __eGAMESTATE {
		__MAINMENU = 0,
		__MAINVIEW = 1,
		__ENDOFROUND = 2
	};

	__eGAMESTATE mGameState;

	void mUpdate();
	void mClear();
	void mReset();

	void mSetTurnLeft(int, bool);	// car movement, given car id
	void mSetTurnRight(int, bool);
	void mSetAccelerate(int, bool);
	void mSetBrake(int, bool);

	bool mTestTrackCollision(int);

private:
	sf::Clock clkUpdate;	// A timer to update the game
	sf::Clock clkRoundTimer;	// A timer for each round of ai cars to go around the track
	bool bAutoRun;		// Autorun to next generation flag
	bool bShowCarDebug;	// Show car debug info flag

	float mStartingX;	// Starting x,y position for cars
	float mStartingY;
	float mStartingFacing; // Starting facing for cars

	int mCurrentTrack;	// The current track number
	bool bTrackRotation; // True = Rotate through tracks, False = Stay on same track each generation


	// Cars
public:
	Car mCars[MAX_CARS];
	int mNumCars;
	int mRoundTime;
	int mFitParents;
	int mNonFitParents;
	int mMutationRate;		// (in 1/100% increments, so 0-100 = 0-1%)

	int mHighlightedCar;	// Which car is currently highlighted




private:



	// AI
public:
	AIManager mAIManager;

private:


	// Setup Menu
public:


private:
	enum __eSETUPMENU {
		__eSETUPNUMCARS = 0,
		__eSETUPROUNDTIME = 1,
		__eSETUPFITPARENTS = 2,
		__eSETUPNONFITPARENTS = 3,
		__eSETUPMUTATIONRATE = 4,
		__eSETUPTRACK = 5
	};
	int mSetupSelectedRow; // UI row selected for input

	int mSetupNumCars;	// Number of Cars selected by user
	int mSetupRoundTime; // Round time selected by user
	int mSetupFitParents; // Number of fit parents to select for next generations
	int mSetupNonFitParents; // Number of non-fit parents to select for next generations
	int mSetupSelectedTrack; // Track selection (0 = all, other values indicate track number)
	int mSetupMutationRate; // % mutation rate when creating offspring (in 1/100% increments, so 0-100 = 0-1%)
};



#endif // __GAME_H_
