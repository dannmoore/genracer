/*
  File:     AIManager.cpp
  Description: AIManager class methods
  Notes: The AIManager handles all AIMembers, determines parents, breeds and mutates new generations of AIMembers

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "AIManager.h"


AIManager::AIManager() {
	mNumAIMembers = 0;
	elapsed_time = 0;
}


AIManager::~AIManager() {


}


// Reset our AIManager and all of the AIMembers
void AIManager::mReset(int num_ai, std::mt19937_64 *r) {
	mNumAIMembers = num_ai;
	mCurrentGeneration = 0;

	if (__AI_PRINT_DEBUG__) std::cout << "Generation " << mCurrentGeneration << ": Loading " << mNumAIMembers << " AI Members..." << std::endl;

	// Clear out our sorting array
	for (int i = 0; i < MAX_AIMEMBERS; i++) {
		SortedAIMembers[i] = NULL;
	}

	for (int i = 0; i < mNumAIMembers; i++) {
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << i << ":" << std::endl;
		AIMembers[i].RandomizeAI(r);
		if(__AI_PRINT_DEBUG__) AIMembers[i].PrintDebugInfo();
		SortedAIMembers[i] = &AIMembers[i];
	}
}



// Set measurements for the specified ai member id
void AIManager::SetMeasurements(int id, float m0, float m1, float m2, float m3, float m4) {
	AIMembers[id].SetMeasurements(m0, m1, m2, m3, m4);
}


// Set the elapsed time for this round
void AIManager::SetElapsedTime(int time_elapsed) {
	elapsed_time = time_elapsed;
}


int AIManager::GetCurrentGeneration() {
	return(mCurrentGeneration);
}


// Based on the current measurements of each ray, determine if the AI will accelerate
bool AIManager::GetAccel(int id) {
	return(AIMembers[id].GetAccel());
}

bool AIManager::GetBrake(int id) {
	return(AIMembers[id].GetBrake());
}

bool AIManager::GetTurnRight(int id) {
	return(AIMembers[id].GetTurnRight());
}

bool AIManager::GetTurnLeft(int id) {
	return(AIMembers[id].GetTurnLeft());
}

void AIManager::AddDistance(int id, float dist) {
	AIMembers[id].AddDistance(dist);
}


float AIManager::GetDistance(int id) {
	return(AIMembers[id].GetDistance());
}


float AIManager::GetMeasurement(int id, int index) {
	return(AIMembers[id].GetMeasurement(index));
}

float AIManager::GetThreshold(int id, int index) {
	return(AIMembers[id].GetThreshold(index));
}


int AIManager::GetFitness(int id) {
	return(AIMembers[id].GetFitness());
}



void AIManager::ToggleMarkForNextGen(int id) {
	if (AIMembers[id].GetFitness() == AIManager::__eAIFITNESS::__NONE) {
		AIMembers[id].MarkFitness(AIManager::__eAIFITNESS::__USER);
	}
	else {
		AIMembers[id].MarkFitness(AIManager::__eAIFITNESS::__NONE);
	}
}


struct AIMemberComparator {
	AIMemberComparator(int elapsed) { this->time_elapsed = elapsed; }
	bool operator () (AIMember *a1, AIMember *a2) {
		return((a1->GetDistance() / time_elapsed) > (a2->GetDistance() / time_elapsed));
	}

	int time_elapsed;
};



/*
SelectFittest():
	Selects the top fittest AI members

Params:
	n = The number of AI to select, will select the top n members
*/
void AIManager::SelectFittest(int n) {
	// Let's first sort our AIMembers based on distance traveled / time
	int arraysize = sizeof(SortedAIMembers) / sizeof(SortedAIMembers[0]);
	std::sort(SortedAIMembers, SortedAIMembers + mNumAIMembers, AIMemberComparator(elapsed_time));

	// Now select the top n members
	for (int i = 0; i < n; i++) {
		SortedAIMembers[i]->MarkFitness(AIManager::__eAIFITNESS::__FIT);
	}
}


/*
SelectRandomNonFit():
	Selects random AI members that were not the fittest

Params:
	n = The number of Random AI to select
	r = RNG
*/
void AIManager::SelectRandomNonFit(int n, std::mt19937_64 *r) {
	bool found;
	int index;

	std::uniform_int_distribution<int> unii(0, mNumAIMembers - 1);

	// Let's select some AIMembers at random that will also be used to breed the next generation
	for (int i = 0; i < n; i++) {
		found = false;
		while (!found) {
			// choose random member
			index = unii(*r);

			// if this member is not already selected for low or high fitness
			if (AIMembers[index].GetFitness() == AIManager::__eAIFITNESS::__NONE) {
				AIMembers[index].MarkFitness(AIManager::__eAIFITNESS::__UNFIT);
				found = true;
			}

			// otherwise we loop and choose another
		}
	}
}



/*
	CreateNextGeneration() 
		Creates the next generation of AIMembers based on the selected parents

	Params:
		mutaterate - the rate of mutation, 0-100
		r - a pointer to our random number generator
*/
void AIManager::CreateNextGeneration(int mutaterate, std::mt19937_64 *r) {
	// First save the selected fit and non-fit members to be parents for the next generation
	std::vector<AIMember> parents(MAX_AIMEMBERS);
	int num_parents = 0;
	int first_parent = -1;
	int second_parent = -1;

	for (int i = 0; i < mNumAIMembers; i++) {
		if (AIMembers[i].GetFitness() != AIManager::__eAIFITNESS::__NONE) {
			parents[num_parents] = AIMembers[i];
			num_parents++;
		}
	}

	if (num_parents < 2) {	// sanity check
		// If we don't have enough parents just select the first two as parents
		std::cout << "Error in AIManager::CreateNextGeneration(), less than two parents found!  Choosing [0] and [1] as parents!" << std::endl;
		parents[0] = AIMembers[0];
		parents[1] = AIMembers[1];
		num_parents = 2;
		return;
	}

	std::uniform_int_distribution<int> unii(0, num_parents - 1);

	// Clear out our sorting array
	for (int i = 0; i < MAX_AIMEMBERS; i++) {
		SortedAIMembers[i] = NULL;
	}


	// Increment the generation counter
	mCurrentGeneration++;


	// Loop through to generate the number of AIMembers we require
	if (__AI_PRINT_DEBUG__) std::cout << "Generation " << mCurrentGeneration << ": Loading " << mNumAIMembers << " AI Members..." << std::endl;
	for (int i = 0; i < mNumAIMembers; i++) {
		// Select a pair to be parents for this AIMember at random
		first_parent = unii(*r);
		do {
			second_parent = unii(*r);
		} while (first_parent == second_parent);


		// Breed a new AIMember from the selected parents
		if (__AI_PRINT_DEBUG__) std::cout << "Parents " << first_parent << ", " << second_parent << " spawned AIMember: " << i << ": " << std::endl;
		BreedNewAIMember(&parents[first_parent], &parents[second_parent], i, mutaterate, r);
		if (__AI_PRINT_DEBUG__) AIMembers[i].PrintDebugInfo();
		SortedAIMembers[i] = &AIMembers[i];

		// Reset parent indicies for next pair
		first_parent = -1;
		second_parent = -1;
	}
}



/*
BreedNewAIMember():
	Breeds a new AI member using the given parents

Params:
	parent1, parent2 - AI members to breed together
	id - AIMembers[] array index in which place the new AI
	mutaterate - rate of mutation to apply
	r - RNG
*/
void AIManager::BreedNewAIMember(AIMember *parent1, AIMember *parent2, int id, int mutaterate, std::mt19937_64 *r) {
	int index;
	int movement = 0;
	float mutatechance;

	// Reset measurements
	AIMembers[id].SetMeasurements(0, 0, 0, 0, 0);

	// Reset fitness
	AIMembers[id].MarkFitness(AIManager::__eAIFITNESS::__NONE);

	// Reset distance traveled
	AIMembers[id].SetDistance(0);


	// RNG
	std::uniform_int_distribution<int> unii_parent(0, 1);	// 50% parental inheritence
	std::uniform_real_distribution<float> unif_mutate(0, 100);	// 0 <= values < 100
	std::uniform_int_distribution<int> unii_movement(0, 2);
	std::uniform_int_distribution<int> unii_turn(-1, 1);
	std::uniform_int_distribution<int> unii_color(-20, 20); // color variance when mutating
	std::uniform_int_distribution<int> unii_threshold(-64, 64); // threshold variance when mutating


	// For each color select one of the parents to inherit from
	// red
	index = unii_parent(*r);
	if (index == 0) {
		AIMembers[id].color_r = parent1->color_r;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited red color value from first parent" << std::endl;
	}
	else {
		AIMembers[id].color_r = parent2->color_r;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited red color value from second parent" << std::endl;
	}

	// Now check to see if we should mutate
	mutatechance = unif_mutate(*r);
	if (mutatechance < mutaterate) {
		// add a color variance mutation
		AIMembers[id].color_r += unii_color(*r);
		if (AIMembers[id].color_r < 20) AIMembers[id].color_r = 20; // Constrain color to 20-255 to prevent solid black cars
		if (AIMembers[id].color_r > 255) AIMembers[id].color_r = 255;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " mutated red color value!" << std::endl;
	}

	// green
	index = unii_parent(*r);
	if (index == 0) {
		AIMembers[id].color_g = parent1->color_g;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited green color value from first parent" << std::endl;
	}
	else {
		AIMembers[id].color_g = parent2->color_g;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited green color value from second parent" << std::endl;
	}

	// Now check to see if we should mutate
	mutatechance = unif_mutate(*r);
	if (mutatechance < mutaterate) {
		// add a color variance mutation
		AIMembers[id].color_g += unii_color(*r);
		if (AIMembers[id].color_g < 20) AIMembers[id].color_g = 20; // Constrain color to 20-255 to prevent solid black cars
		if (AIMembers[id].color_g > 255) AIMembers[id].color_g = 255;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " mutated green color value!" << std::endl;
	}

	// blue
	index = unii_parent(*r);
	if (index == 0) {
		AIMembers[id].color_b = parent1->color_b;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited blue color value from first parent" << std::endl;
	}
	else {
		AIMembers[id].color_b = parent2->color_b;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited blue color value from second parent" << std::endl;
	}

	// Now check to see if we should mutate
	mutatechance = unif_mutate(*r);
	if (mutatechance < mutaterate) {
		// add a color variance mutation
		AIMembers[id].color_b += unii_color(*r);
		if (AIMembers[id].color_b < 20) AIMembers[id].color_b = 20; // Constrain color to 20-255 to prevent solid black cars
		if (AIMembers[id].color_b > 255) AIMembers[id].color_b = 255;
		if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " mutated blue color value!" << std::endl;
	}





	// For each threshold select one of the parents to inherit from
	for (int j = 0; j < 5; j++) {
		index = unii_parent(*r);
		if (index == 0) {
			AIMembers[id].thresholds[j] = parent1->thresholds[j];
			if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited threshold " << j << " from first parent" << std::endl;
		}
		else {
			AIMembers[id].thresholds[j] = parent2->thresholds[j];
			if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " inherited threshold " << j << " from second parent" << std::endl;
		}

		// Now check to see if we should mutate
		mutatechance = unif_mutate(*r);
		if (mutatechance < mutaterate) {
			// add a threshold variance mutation
			AIMembers[id].thresholds[j] += unii_threshold(*r);
			if (AIMembers[id].thresholds[j] < 1) AIMembers[id].thresholds[j] = 1; // Constrain threshold to 1-500 
			if (AIMembers[id].thresholds[j] > 500) AIMembers[id].thresholds[j] = 500;
			if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " mutated threshold " << j << "!" << std::endl;
		}
	}





	// actions
	// For each action select one of the parents to inherit from
	for (int line0 = 0; line0 < 5; line0++) {	// line 0
		for (int thrs0 = 0; thrs0 < 2; thrs0++) {	// < or >
			for (int line1 = 0; line1 < 4; line1++) {	// line 1
				for (int thrs1 = 0; thrs1 < 2; thrs1++) {	// < or >
					for (int line2 = 0; line2 < 3; line2++) {	// line 2
						for (int thrs2 = 0; thrs2 < 2; thrs2++) {	// < or >
							for (int line3 = 0; line3 < 2; line3++) {	// line 3
								for (int thrs3 = 0; thrs3 < 2; thrs3++) {	// < or >
									for (int thrs4 = 0; thrs4 < 2; thrs4++) {	// < or >
										// select one of the parents to inherit from
										index = unii_parent(*r);
										if (index == 0) {
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel =
												parent1->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel;
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake =
												parent1->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake;
										}
										else {
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel =
												parent2->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel;
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake =
												parent2->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake;
										}

										// turning
										index = unii_parent(*r);
										if (index == 0) {
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir =
												parent1->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir;
										}
										else {
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir =
												parent2->actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir;
										}


										// Now check to see if we should mutate
										mutatechance = unif_mutate(*r) * ACTION_MUTATE_FUDGE_FACTOR;
										if (mutatechance < mutaterate) {
											// Do a simple mutation of randomizing these actions
											// acceleration and braking are exclusve, or both can be off (coasting)
											movement = unii_movement(*r);

											switch (movement) {
												case 0: // accelerate
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = true;
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = false;
													break;
												case 1: // brake
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = false;
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = true;
													break;
												case 2: // neither
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].accel = false;
													AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].brake = false;
													break;
											}

											// Turning can be -1 (left), 0 (straight), or 1 (right)
											AIMembers[id].actions[line0][thrs0][line1][thrs1][line2][thrs2][line3][thrs3][thrs4].turndir = unii_turn(*r);

											if (__AI_PRINT_DEBUG__) std::cout << "AIMember " << id << " mutated actions!" << std::endl;
										}
									}	// < or >
								}	// < or >
							}	// line 3
						}	// < or >
					}	// line 2
				}	// < or >
			}	// line 1
		}	// < or >
	}	// line 0



}




