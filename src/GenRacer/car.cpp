/*
  File:    car.cpp
  Description: Car class
  Notes: A single car and associated status information

  Copyright (C) 2019 Dann Moore.  All rights reserved.
*/

#include "car.h"


Car::Car() {
	speed = 0.0f;
	x = 0.0f;
	y = 0.0f;
	facing = 0.0f;
	active = false;
	crashed = false;
	accelerating = false;
	braking = false;
	turndir = 0;
	r = 255;
	g = 255;
	b = 255;
}


Car::~Car() {

}