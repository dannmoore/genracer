GenRacer
Copyright (C) 2019 Dann Moore.  All rights reserved.


Overview:

GenRacer is an application styled after a top-down 2D racing game that utilizes a genetic algorithm to teach AI cars how to race.  It includes various options to tweak the evolution of AI racers and a number of tracks to test on.  GenRacer is written in C++ with SFML 2.5.1 using Visual Studio 2017.


Setup Menu:

Upon launching the application you will be presented with the following setup options:

Number Of AI Cars - This controls the number of AI cars in each generation.
Generation Round Time - The round time is the length in seconds of time each generation is given to race before stopping.
Fit Parents To Select - This is the fittest number of AI cars to act as parents for the next generation.  For example, if this number is set to 5, the application will select the top 5 fittest AI cars as parents.  Note that parents can still be chosen manually at the end of each round if desired.
Non-Fit Parents To Select - This is the number of additional AI cars chosen at random to act as parents for the next generation.
Mutation Rate - Each new generation of AI cars will have up to this amount of their "chromosomes" mutated.
Track Selection - This controls the track the AI cars will race on, or if set to "All" will rotate through each included track.

Follow the on-screen instructions to set the options as desired and press the spacebar to begin.


Race View:

Once begun you will see the selected racetrack(s), individual AI cars and various information displayed on the screen:

- At the top of the screen is information pertaining to the current generation of AI cars, as well as options selected in the Setup Menu.
- Directly below is information about the currently selected AI car including sensor information, current actions, etc.
- In the top-right of the screen is the remaining Round Time for the current generation
- At the bottom-right of the screen is a list of controls available.

While the simulation is running you can perform the following actions:

Left Click - Select an AI car to view information about
CTRL-R - Reset all AI cars back to generation 0 (completely reset using the same settings from the Setup Menu)
A - Toggles AutoRun on/off, allowing AI cars to breed new generations when the Round Timer reaches zero without further intervention
I - Toggles the display of AI car information on/off
ESC - Returns to the Setup Menu

After the Round Timer expires, the application will pause allowing you to select or de-select AI cars as parents for the next generation.  In addition to the above actions you can also perform the following:

M - Mark/Unmark the currently selected AI car as a parent for the next generation
Space - Continue and breed the next generation of AI cars


Genetic Algoritm Overview:

As the application runs you should notice the AI cars learning how to race around a track as more generations are created.  How exactly do the cars drive and what information do they have to take action upon?  Each AI car has the following information:

- A set of five rays drawn from the front of the car to the furthest wall of the track and stored as a measurement of pixels
- A similar set of thresholds of each ray measurement in which the car will take action.  For example if the measurement of a ray cast directly in front of the car is greater than 100 pixels the AI will perform a certain action.  If it is less than 100 pixels it may perform a different action.
- A set of actions for each permutation of ray measurements and thresholds.  An AI car may accelerate, brake, turn left, turn right, and/or go straight.
- Additionaly for visualization each car also has a color assigned to it.  When parent AI cars breed a new AI car, the color values can be inherited by either parent.

At the start, generation 0, AI cars will be completely randomized.  At the end of each round a set of cars will be selected to become parents to the next generation.  This will be a mixture of the fittest cars as determined by the total distance driven, and random "non-fit" cars for genetic diversity.  The parents will then be paired randomly and breed a new AI car.  Finally the new AI car is mutated according to the mutation rate set.


Notes about the tracks:

- Racetracks consist of three files - a track displayed on the screen, a mask for collision detection, and a text file for starting line position.  
- For example, track1.png is a 1920x1080 image displayed on the screen, track1_mask.png is the collision detection mask where cars will collide with any non-transparent pixel, and track1.txt contains the starting line position for the cars.
- Track text format, one number per line:
  123 // X position of the cars' starting point
  45  // Y position of the cars' starting point
  90  // Angle of facing in degrees of the cars at the start
- You may make new tracks by following the format, however the number of tracks is currently hardcoded to 4.
- There is no support for alternate paths or "figure 8" style path crossing, and the AI cars will likely not be able to learn this style of track



Other Notes:
- Text font used is Copyright (c) 2016 VileR, licensed under Creative Commons Attribution-ShareAlike 4.0 International License, as originally found at https://int10h.org/oldschool-pc-fonts/




